<?php

namespace Vdshop\PetInsurance\Service;

use Vdshop\BaseInsurance\Model\AbstractResponse;

class broken extends AbstractResponse
{
    const RESPONSE_OBJECT = '';

    public function getRefExt()
    {
        $data = $this->getData();
        $result = '';

        if (empty($data)) {
            return '';
        }
        if (!$this->getErrors()) {
            if (isset($data[static::RESPONSE_OBJECT]['RESPUESTA']['REF_EXT'])) {
                $result = $data[static::RESPONSE_OBJECT]['RESPUESTA']['REF_EXT'];
            }
        }

        return $result;
    }

    public function getErrors()
    {
        $data = $this->getData();
        $result = false;

        if (empty($data)) {
            return __('No Data');
        }

        if (isset($data[static::RESPONSE_OBJECT]['RESPUESTA']['ESTADO'])) {
            if ($data[static::RESPONSE_OBJECT]['RESPUESTA']['ESTADO'] !== 'OK') {
                if (isset($data[static::RESPONSE_OBJECT]['RESPUESTA']['CONTROLES_NEGOCIO'])) {
                    $result = $data[static::RESPONSE_OBJECT]['RESPUESTA']['CONTROLES_NEGOCIO'];
                } else {
                    $result = __('There are some errors');
                }
            }
        } else {
            $result = __('There are some errors');
        }
        return $result;
    }

    public function getPremiums()
    {
        $data = $this->getData();

        $result = [];

        if (empty($data)) {
            return [];
        }

        if (!$this->getErrors()) {
            if (isset($data[static::RESPONSE_OBJECT]['RESPUESTA'])) {
                if (isset($data[static::RESPONSE_OBJECT]['RESPUESTA']['PRIMAS'])) {
                    $root = $data[static::RESPONSE_OBJECT];
                } else {
                    $root = $data[static::RESPONSE_OBJECT]['RESPUESTA'];
                }

                foreach ($root as $respuesta) {
                    $result[] = [
                        'annual'=> number_format($this->sanitizeNumber($respuesta['PRIMAS']['PANUAL']), 2),
                        'semesters' => [
                            '1' => number_format($this->sanitizeNumber($respuesta['PRIMAS']['PSEMESTRE1']), 2),
                            '2' => number_format($this->sanitizeNumber($respuesta['PRIMAS']['PSEMESTRE2']), 2),
                            'total' => number_format($this->sanitizeNumber($respuesta['PRIMAS']['PTOTALSEMESTRE']), 2),
                        ],
                        'trimester' => [
                            '1' => number_format($this->sanitizeNumber($respuesta['PRIMAS']['PTRIMESTRE1']), 2),
                            '2' => number_format($this->sanitizeNumber($respuesta['PRIMAS']['PTRIMESTRE2']), 2),
                            'total' => number_format($this->sanitizeNumber($respuesta['PRIMAS']['PTOTALTRIMESTRE']), 2),
                        ],
                    ];
                }
            }
        }

        return $result;
    }
}
