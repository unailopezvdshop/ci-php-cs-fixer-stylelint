module.exports = {
    "rules": {
        "block-no-empty": null,
        "color-no-invalid-hex": true,
        "comment-empty-line-before":
            ["always", {
                "ignore": ["stylelint-commands", "after-comment"]
            }],
        "declaration-colon-space-after": "always",
        "indentation": 4,
        "max-empty-lines": 1,
        "rule-empty-line-before":
            ["always", {
                "except": ["first-nested"],
                "ignore": ["after-comment"]
            }],
        "unit-whitelist": ["em", "rem", "%", "s", "px", "vh", "vw", "deg"],
        "selector-list-comma-newline-after": "always",
        "length-zero-no-unit": true
    }
}